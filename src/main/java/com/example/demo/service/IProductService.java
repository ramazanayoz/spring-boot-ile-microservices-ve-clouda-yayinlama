package com.example.demo.service;

import com.example.demo.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IProductService {

    public Product saveProduct(Product product);
    public void deleteProduct(Long productId);
    public List<Product> findAllProducts();
}
